#ifndef __GST_FAAC_H__
#define __GST_FAAC_H__

#include <gst/gst.h>
#include <gst/base/gstadapter.h>
#include <gst/audio/audio.h>
#include <faac.h>

G_BEGIN_DECLS

#define GST_TYPE_FAAC \
  (gst_faac_get_type ())
#define GST_FAAC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_FAAC, GstFaac))
#define GST_FAAC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_FAAC, GstFaacClass))
#define GST_IS_FAAC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_FAAC))
#define GST_IS_FAAC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_FAAC))

typedef struct _GstFaac GstFaac;
typedef struct _GstFaacClass GstFaacClass;

struct _GstFaac {
  GstElement element;

  /* pads */
  GstPad *srcpad, *sinkpad;

  /* stream properties */
  gint samplerate,
       channels,
       format,
       bps,
       bitrate,
       profile,
       shortctl,
       outputformat;
  gboolean tns,
           midside;
  gulong bytes,
         samples;

  /* FAAC object */
  faacEncHandle handle;

  /* cache of the input */
  GstAdapter *adapter;
  /* offset of data to be encoded next */
  guint offset;

  guint64  last_ts;
  guint64  encoder_frames;
};

struct _GstFaacClass {
  GstElementClass parent_class;
};

GType gst_faac_get_type (void);

G_END_DECLS

#endif /* __GST_FAAC_H__ */
