CFLAGS=\
	-DGST_PACKAGE_NAME='"GStreamer"' \
	-DGST_PACKAGE_ORIGIN='"http://gstreamer.net"' \
	-DPACKAGE='"faac"' \
	-DVERSION='"0.0"' -DHAVE_USER_MTU -Wall -Wimplicit -g \
	$(shell pkg-config --cflags gstreamer-audio-0.10) -I.
	
LDFLAGS=$(shell pkg-config --libs gstreamer-audio-0.10) -lfaac
INSTALL_DIR=/usr/lib64/gstreamer-0.10/
PWD=$(shell pwd)

libgstfaac.la:gstfaac.lo
	libtool --mode=link gcc -module -shared -export-symbols-regex gst_plugin_desc $(LDFLAGS) -o $@ $+ -avoid-version -rpath $(INSTALL_DIR)

%.lo: %.c %.h
	libtool --mode=compile gcc $(CFLAGS) -fPIC -c -o $@ $<
	
.PHONY: install

install: .libs/libgstfaac.so
	libtool --mode=install install $< $(INSTALL_DIR)
	
link:.libs/libgstfaac.so
	ln -sf $(PWD)/$< $(INSTALL_DIR)

clean:
	rm -rf *.o *.lo *.a *.la .libs
